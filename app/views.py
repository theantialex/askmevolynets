from django.shortcuts import render
from django.core.paginator import Paginator
from .models import *

questions_list = []
for i in range(1, 30):
  questions_list.append({
    'title': 'title' + str(i),
    'id': i,
    'text': 'text' + str(i),
    'tag1': 'tag' + str(i),
    'tag2': 'tag' + str(i + 1),
    'answer_num': str(i),
    'vote': str(i + 5)
})

hot_questions_list = []
for i in range(1, 30):
  hot_questions_list.append({
    'title': 'hot_title' + str(i),
    'id': i,
    'text': 'text' + str(i),
    'tag1': 'tag' + str(i),
    'tag2': 'tag' + str(i + 1),
    'answer_num': str(i),
    'vote': str(i + 5)
})

tagged_questions_list = []
for i in range(1, 30):
      tagged_questions_list.append({
          'title': 'tagged_title' + str(i),
          'id': i,
          'text': 'text' + str(i),
          'tag1': 'tag' + str(i),
          'tag2': 'tag' + str(i + 1),
          'answer_num': str(i),
          'vote': str(i + 5)
      })

answers_list = []
for i in range(1, 10):
    answers_list.append({
        'id': i,
        'text': 'text' + str(i),
        'vote': str(i)
})

members = ['Hanna Montana', 'Min Yoongi', 'Xiao Zhan', 'Wang Yibo', 'Kim Dahyun']

popular_tags = [{'name': 'Serendipity', 'color': 'red'},
                {'name': 'Banter', 'color': 'green'},
                {'name': 'Fighting', 'color': 'violet'},
                {'name': 'Muster', 'color': 'yellow'},
                {'name': 'BangBangCon', 'color': 'blueviolet'}]

alert = ""


def get_paginator(request, obj_list, size):
    paginator = Paginator(obj_list, size)
    page = request.GET.get('page')
    return paginator.get_page(page)


def index(request):
    questions_list = Question.objects.all()
    questions = get_paginator(request, questions_list, 10)
    return render(request, 'index.html', {'questions': questions, 'popular_tags': popular_tags, 'members': members})


def hot_questions(request):
    hot_questions = get_paginator(request, hot_questions_list, 10)
    return render(request, 'select_questions.html', {'questions': hot_questions, 'type': 'hot',
                                                     'popular_tags': popular_tags, 'members': members})


def one_question(request, pk):
    question = Question.objects.get(id=pk)
    answers_list_1 = Answer.objects.filter(question__id=pk)
    answers = get_paginator(request, answers_list_1, 5)
    return render(request, 'question.html', {"question": question, "answers": answers,

                                             'popular_tags': popular_tags, 'members': members})


def login(request):
    return render(request, 'login.html', {'alert': alert})


def signup(request):
    return render(request, 'signup.html', {'alert': alert, 'popular_tags': popular_tags, 'members': members})


def ask(request):
    return render(request, 'ask.html', {'popular_tags': popular_tags, 'members': members})


def settings(request):
    return render(request, 'settings.html', {'popular_tags': popular_tags, 'members': members})


def tagged_questions(request, str):
    tagged_questions = get_paginator(request, tagged_questions_list, 10)
    return render(request, 'select_questions.html', {'questions': tagged_questions, 'type': 'tagged', 'popular_tags': popular_tags, 'members': members})