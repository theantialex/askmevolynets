from django.db import models
from django.contrib.auth.models import User


class QuestionManager(models.Manager):

 def tags(self):
     return Tag.objects.filter(question__id=self.id)

 def answer_num(self):
     return Answer.objects.filter(question__id=self.id).count()


class Question(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    date = models.DateField()
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    objects = QuestionManager()

    def __str__(self):
        return self.title


class Answer(models.Model):
    text = models.TextField()
    date = models.DateField()
    author = models.ForeignKey('Profile', on_delete=models.CASCADE)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)

    def __str__(self):
        return self.text


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='static/img')

    def __str__(self):
        return self.user.__str__()


class QuestionLike(models.Model):
    like = models.IntegerField()
    question = models.ForeignKey('Question', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.like) + ' ' + self.question.__str__()


class AnswerLike(models.Model):
    like = models.IntegerField()
    answer = models.ForeignKey('Answer', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.like) + ' ' + self.question.__str__()


class Tag(models.Model):
    name = models.CharField(max_length=100)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
